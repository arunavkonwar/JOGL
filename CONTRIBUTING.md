# Contributing

We welcome contributions !

* API first, head to our API documentation for a current view of the API https://documenter.getpostman.com/view/8688524/SWE84xMg?version=latest
* If you want to add code, fork from the branch `develop` and merge request to the branch `develop` ! Any request to master or staging will be automatically rejected.

When contributing to this repository, on already planned issues, or known bugs. If there's no more, feel free to go wild !  

Please note we have a code of conduct, please follow it in all your interactions with the project.

## Code Stack
The code is split between two repository, Frontend (React.JS) and Backend (Ruby on Rails).

- Backend: https://gitlab.com/JOGL/backend-v0.1
- Frontend: https://gitlab.com/JOGL/frontend-v0.1

We use Algolia as an external search service in order to speed up the Frontend and benefit from their amazing search engine. If working on the Backend, you will need to create a Free dev account on Algolia. 

## Working on the Backend? Create a free Algolia account
As per Algolia pricing, everyone can create a free account: https://www.algolia.com/users/sign_up  (It might say it's 14 days, but as long as you don't upgrade you can keep your free account !)
Once created, you will need to setup your API keys, then follow the Backend README on how to set it up.
If all fails, contact us so that we can help and if need be provide you with some API keys.

## Working on the Frontend? Use the .env file included
In the Frontend repository are .env file containing some dev account Algolia keys that you can use. If you work on Both the backend and the frontend, we recommend you setup your own Algolia account to be able to run your tests productively.

## Code philosophy

Here is the philosophy (I know it repeats!):
1) API first, head to our API documentation for a current view of the API https://documenter.getpostman.com/view/8688524/SWE84xMg?version=latest
2) If what you need is missing from our API, then you'll have to add either it to our backend: https://gitlab.com/JOGL/backend-v0.1 or add an issue to add it to the Backend.
3) If you want to add code, fork from the branch `develop` and merge request to the branch `develop` ! Any request to master or staging will be automatically rejected.
4) DRY !
5) Object Oriented thinking, if you think you should make a new component for this, then you should indeed! Modular code is the way forward!

Some technical aspect:
1) The code is supported by React
2) The framework for rendering is Bootstrap 4.0
3) If you need an API to test things you can point to: https://jogl-backend-dev.herokuapp.com
4) If you prefer to work locally, test our dockerstack: https://gitlab.com/JOGL/JOGL

## Pull Request Process

1. Ensure any install or build dependencies are removed before the end of the layer when doing a
   build.
2. Update the README.md with details of changes to the interface, this includes new environment
   variables, exposed ports, useful file locations and container parameters.
3. Increase the version numbers in any examples files and the README.md to the new version that this
   Pull Request would represent. The versioning scheme we use is [SemVer](http://semver.org/).
4. You may merge the Pull Request in once you have the sign-off of two other developers, or if you
   do not have permission to do that, you may request the second reviewer to merge it for you.

## Code of Conduct

### Our Pledge

In the interest of fostering an open and welcoming environment, we as
contributors and maintainers pledge to making participation in our project and
our community a harassment-free experience for everyone, regardless of age, body
size, disability, ethnicity, gender identity and expression, level of experience,
nationality, personal appearance, race, religion, or sexual identity and
orientation.

### Our Standards

Examples of behavior that contributes to creating a positive environment
include:

* Using welcoming and inclusive language
* Being respectful of differing viewpoints and experiences
* Gracefully accepting constructive criticism
* Focusing on what is best for the community
* Showing empathy towards other community members

Examples of unacceptable behavior by participants include:

* The use of sexualized language or imagery and unwelcome sexual attention or
advances
* Trolling, insulting/derogatory comments, and personal or political attacks
* Public or private harassment
* Publishing others' private information, such as a physical or electronic
  address, without explicit permission
* Other conduct which could reasonably be considered inappropriate in a
  professional setting

### Our Responsibilities

Project maintainers are responsible for clarifying the standards of acceptable
behavior and are expected to take appropriate and fair corrective action in
response to any instances of unacceptable behavior.

Project maintainers have the right and responsibility to remove, edit, or
reject comments, commits, code, wiki edits, issues, and other contributions
that are not aligned to this Code of Conduct, or to ban temporarily or
permanently any contributor for other behaviors that they deem inappropriate,
threatening, offensive, or harmful.

### Scope

This Code of Conduct applies both within project spaces and in public spaces
when an individual is representing the project or its community. Examples of
representing a project or community include using an official project e-mail
address, posting via an official social media account, or acting as an appointed
representative at an online or offline event. Representation of a project may be
further defined and clarified by project maintainers.

### Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported by contacting the project team at [INSERT EMAIL ADDRESS]. All
complaints will be reviewed and investigated and will result in a response that
is deemed necessary and appropriate to the circumstances. The project team is
obligated to maintain confidentiality with regard to the reporter of an incident.
Further details of specific enforcement policies may be posted separately.

Project maintainers who do not follow or enforce the Code of Conduct in good
faith may face temporary or permanent repercussions as determined by other
members of the project's leadership.

### Attribution

This Code of Conduct is adapted from the [Contributor Covenant][homepage], version 1.4,
available at [http://contributor-covenant.org/version/1/4][version]

[homepage]: http://contributor-covenant.org
[version]: http://contributor-covenant.org/version/1/4/
